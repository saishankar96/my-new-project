package CreditCard;

public class Address {

	private String state;
	private String city;
	private String country;
	private String homeAdres;
	
	public Address() 
	{
	state = " ";
	city = " ";
	country = " ";
	homeAdres = " ";
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHomeAdress() {
		return homeAdres;
	}

	public void setHomeAdress(String homeAdress) {
		this.homeAdres = homeAdress;
	}

	
}
