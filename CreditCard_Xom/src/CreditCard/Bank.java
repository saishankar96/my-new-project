package CreditCard;

public class Bank {
	
	private String bankName;
	private String branchAddress;	
	private String branchName;

	public Bank() {
		bankName = " ";
		branchAddress = " ";
		branchName =" ";
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	
}
