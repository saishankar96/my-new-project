package CreditCard;

public class Request {
private Bank bank;
private Customer Customer;
private Address address;

public Request() {
	super();
	// TODO Auto-generated constructor stub
}

public Bank getBank() {
	return bank;
}

public void setBank(Bank bank) {
	this.bank = bank;
}

public Customer getCustomer() {
	return Customer;
}

public void setCustomer(Customer customer) {
	Customer = customer;
}

public Address getAddress() {
	return address;
}

public void setAddress(Address address) {
	this.address = address;
}

}