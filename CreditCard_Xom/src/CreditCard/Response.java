package CreditCard;

public class Response
{
	private String eligibility;
	private String validity;
	private String cardStatus;
	
	public Response() {
		eligibility = " ";
		validity = " ";
		cardStatus = " ";
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}


}
