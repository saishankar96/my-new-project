package CreditCard;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Customer {
	private String customerId;
	private Date dateOfBirth;
	private String customerAddress ;
	private String customerName;
	private String employmentType ;
	private double income;
	private int creditScore ;
	private  int age;
	private String phoneNumber;
	private String tanNumber;
	private  List<String> message;
	
	public Customer() 
	{
		 phoneNumber = " ";
      tanNumber = " ";
      customerId = " ";
      customerAddress = " ";
      customerName = " ";
      employmentType = " ";
      message = new ArrayList<String>();

	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	public String getTanNumber() {
		return tanNumber;
	}

	public void setTanNumber(String tanNumber) {
		this.tanNumber = tanNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public int getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<String> getMessage() {
		return message;
	}
	public void removeFromMessages (String argument) {
		message.remove(argument);
	}
	/*
	 * Adds a message.
	 * @param argument The message to add.
	 */
	public void addToMessages (String argument) {
		message.add(argument);
	}
	
	

}
