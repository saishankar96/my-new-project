package CreditCard;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

public class Utility {
	
	public static int  calculateAge(Date dateofbirth)
	{
		int age = 0;
		if (dateofbirth != null) {
			Calendar birth = new GregorianCalendar();
			birth.setTime(dateofbirth);
			Calendar now = new GregorianCalendar();
			now.setTime(new Date());
			int adjust = 0;
			if (now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
				adjust = -1;
			}
			age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
		}
		return age;
		
	}
	/*public static void main (String[]args) throws Exception
	{
		System.out.println(  calculateAge(new SimpleDateFormat("dd-mm-yyyy").parse("10-12-1995")));
	}*/

 
	/*This method is used to validate the phone number*/
	// is this{0} number valid?
	public static boolean isphnumberValid(String phone) {
		boolean flag = false;
		String phoneregex = "(0/91)?[6-9][0-9]{9}";
		Pattern pat = Pattern.compile(phoneregex);
		if(phone !=null) {
			flag = pat.matcher(phone).matches();
		}
		return flag;
	}

	
	/*This method is used to validate the pan number*/
	// is this{0} pan valid?
	public static boolean istanValid(String tan) {
		boolean flag = false;
		String panregex = "[A-Z]{4}[0-9]{5}[A-Z]{1}";
		Pattern pat = Pattern.compile(panregex);
		if(tan !=null) {
			flag = pat.matcher(tan).matches();
		}
		return flag;
	}	

}
